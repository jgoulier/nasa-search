# README #

Création du projet Angular

### Description du projet ###

* Petit projet de formation Angular
* Version : 0.0

### Application à installer pour le faire fonctionner ###

* Installer nodejs et npm version 6 pour la détection des vulnérabilités

### Lignes de commandes ###

* Création du projet Angular
    ng new nasa-search

* Pour installer ou reinstaller tous les modules
    npm install

* Pour lancer la compilation du projet (-o pour qu'il se lance à chaque modification du code)
    ng serve -o

## Ajout du module compodoc ##

* Installation du module compodoc (ce met dans le devDependencies)
    npm i @compodoc/compodoc --save-dev

* Modification du fichier package.json
    Ajouter la ligne suivant dans script
        "doc":"compodoc -p tsconfig.json -s -o"

* Pour lancer compodoc
    npm run doc
	
### URL du projet (si aucune modification du parametrage n'est fait) ###

* [http://localhost:4200/]
